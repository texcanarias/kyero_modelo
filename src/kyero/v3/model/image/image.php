<?php 
namespace inmotek\kyero\v3\model\image;

class image{
    public $image = [];

    public function add(string $url, int $orden){
        $this->image[] = new url($url, $orden);
    }

}

class url{
    public string $url;
    public $_attributes = [];
    public function __construct(string $url, int $orden){
        $this->url = $url;
        $this->_attributes = ['id' => $orden];
    }

}