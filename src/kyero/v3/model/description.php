<?php
namespace inmotek\kyero\v3\model;

/**
* Estructura de datos para almacenar traducciones
 */
class  description{
    public ?string $ca = null;
    public ?string $da = null;
    public ?string $de = null;
    public ?string $en = null;
    public ?string $es = null;
    public ?string $fi = null;
    public ?string $fr = null;
    public ?string $it = null;
    public ?string $nl = null;
    public ?string $no = null;                
    public ?string $pt = null;    
    public ?string $ru = null;
    public ?string $sv = null;
}
