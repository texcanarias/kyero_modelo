<?php
namespace inmotek\kyero\v3\model;

class features{
    public $feature = [];

    public function add(string $feature) : self{
        $this->feature[] = $feature;
        return $this;
    }
}
