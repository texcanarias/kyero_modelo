<?php 
namespace inmotek\kyero\v3\model;

class property{
    /**
      * Mandatory alphanumeric, max 50 characters
		  * Your database or other unique identifier for the property.  
			* Used in conjunction with the <date> tag to determine 
			* if the property should be added or updated   (obligatorio)
     */
    public string $id;

    /**
      * Mandatory datetime, 19 characters
			* Last modified date for this property in your database
			* If a last modified date is not available or accurate, 
			* the feed will only be processed once a week
   		*	Must follow standard datetime format of:
   		*	YYYY-MM-DD HH:MM:SS
   		*	Used in conjunction with the <id> tag 
   		*	to determine if the property should be added or updated  (obligatorio)
     */
    public string $date;

    /**
      * Mandatory alphanumeric, max 255 characters
   	  *	Your customer-visible reference for the property  (obligatorio) 
     */
    public string $ref;

    /**
      * Mandatory numeric, max 8 characters
   		*	Used in conjunction with <price_freq> and <currency> tags
   		*	Enter the price for sale, or rental price per month
   		*	Use whole numbers, no punctuation  (obligatorio)
     */
    public int $price;


    /**
      * Optional alpha: EUR, GBP or USD
   		*	Used in conjunction with <price> tag
   		*	EUR, empty or missing tag for prices in EUR 
   		*	Prices specified in GBP or USD will be converted to EUR
     */
    public string $currency = "EUR";

    /**
      * Mandatory alpha
   		*	'sale' for properties for sale
   		*	'month' for properties for long term rental 
   		*	Used in conjunction with <price> and <currency> tags  (obligatorio)
     */
    public string $price_freq;


    /**
     * Optional numeric
   		*	'1' if price is for part ownership ownership
   		*	Use if <price_freq> = 'sale'
      * Empty, missing tag or '0' for 100% ownership of a property for sale (opcional)
    */
    public ?int $part_ownership = null;


    /**
      * Optional numeric
   		*	'1' if leasehold is being offered
   		*	Use if <price_freq> = 'sale'
   		*	Empty, missing tag or '0' if property freehold is for sale (opcional)
     */
    public ?int $leasehold = null;


    /**
     * Optional numeric
   		*	'1' if property is less than 12 months old
   		*	Used if <price_freq> = 'sale'
   		*	Empty, missing tag or '0' if property is a resale (opcional)
     */
    public ?int $new_build = null;

    /**
     * Mandatory alpha, converted to a Kyero standard property type (obligatorio)
     */
    public string $type;

    /**
      * Mandatory alpha
   		*	Town, preferably a valid Correos location eg: Javea, Nerja
   		*	Converted to a Kyero standard town during import (obligatorio)
     */
    public string $town;

    /**
      * Mandatory alpha
   		*	Province, preferably a valid Correos province eg: Malaga, Valencia
   		*	Converted to a Kyero standard province during import (obligatorio)
     */
    public string $province;

    /**
      * Optional alpha
      *Country, eg: Spain, España, Portugal, France, Francia 
      *If empty or missing, country will default to Spain (opcional)
    */
    public string $country;

    /**
     * Principio de la GEO Location nodo,  recomendable (opcional)
     */
    public ?location $location = null;

    /**
      * Optional alphanumeric, max 50 characters
   		*	Free text to describe a village or urbanisation location 
   		*	which is either too small to be listed by Correos or is a
   		*	'de-facto' place name eg: 'Las Alpujarras' or 'Costa del Sol (opcional)
     */
    public ?string $location_detail = null;

    /**
      * Mandatory numeric, number of bedrooms
   		*	Empty, missing tag or '0' if unknown (opcional)
     */
    public ?int $beds;

    /**
      * Mandatory numeric, number of bathrooms
   		*	Empty, missing tag or '0' if unknown (opcional)
     */
    public ?int $baths;

    /**
      * Mandatory numeric
   		*	'1' if a pool is available
   		*	Empty, missing tag or '0' if unknown (opcional)
     */
    public ?int $pool;

    /**
     * Optional numeric
		 * Constructed and plot area in square metres
   	 * Empty, missing tags or '0' if unknown
     */
    public surface_area $surface_area;

    /**
      * Optional alpha: A, B, C, D, E, F or G
   		* Empty, missing tags or 'X' if unknown
     */
    public ?energy_rating $energy_rating;

    /**
      * Optional, max 255 characters
			* Absolute address of a language-specific web page that contains 
			* further information, specific to this property 
			* Empty or missing tag if language-specific url is not available (Optional)
     */
    public description $url;

    /**
      * Mandatory alphanumeric, property description, no character limit 
			* No HTML, UTF-8 encoded text only
			* Sentence case, no excessive capitalisation 
			* Use &#13; to force a line break in the text (remember, no HTML) 
			* Empty or missing tag if a language-specific description is not available (Optional)
     */
    public description $desc;

    /**
      * Optional alpha, max 35 characters per property feature
			* Can be in Spanish or English
			* No HTML, UTF-8 encoded text only 
			* No line breaks or punctuation
			* Will be automatically translated into each language (opcional) 
     */
    public $features; 


    /**
     * Optional alphanumeric, max 255 characters
		 * Can be used to store additional property-related information
     */
    public $notes;


     /**
      * Images node, maximum of 50 image id's per property
			* Image id's start at 1 and end in 50   
			* URL tag is the absolute FTP or HTTP URL of each image
			* Images are processed and stored on a Kyero server
			* Specify an original image size at least 1280 x 960 pixels
			* Must end with a valid image file type, eg: .jpg
			* This format can be repeated for 50 images - up to <image id="50">
			* The order of the images in your feed will be preserved
			* on Kyero.com.  List the most important image first
      */
      public $images;


    function __construct(\DateTime $fecha_actualizacion){
      $this->date = $fecha_actualizacion->format('Y-m-d H:i:s');
      $this->images = [];
    }
}