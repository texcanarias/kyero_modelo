<?php
namespace inmotek\kyero\v3\model;

class energy_rating{
    /**
     * A</consumption> <!-- Clasificaci�n de consumo - A, B, C, D, E, F, o G (opcional)
     */
    public ?string $consumption = null; 
    
    /**
     * Clasificaci�n de las emisiones - A, B, C, D, E, F, o G (opcional)
     */
    public ?string $emissions = null;
}